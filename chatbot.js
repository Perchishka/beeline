window.onload = init;
let chatInputSubmit = document.createElement("button");
chatInputSubmit.disabled = true;
let chatInputTextField = document.createElement("input");
chatInputTextField.value = '';
chatInputTextField.onkeydown = (e) => e.key === "Enter" ? submit() : null;
let chatMessages = document.createElement("div");
let typingIndicator = null;
let started = false;
let [num1, num2] = [null, null];
const calculate = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
}

function init() {
    let chatWindow = document.createElement("div");
    chatWindow.className = "chatWindow"
    let chatInputContainer = document.createElement("div");
    chatInputContainer.className = "inputContainer";
    chatInputTextField.className = "textField";
    chatInputSubmit.className = "submitButton";
    chatInputSubmit.innerText = "Send";
    chatInputSubmit.onclick = submit;
    chatMessages.className = "messageContainer";
    chatInputTextField.oninput = () => {
        if (chatInputTextField.value === '') {
            chatInputSubmit.disabled = true;
            notTyping();
        } else {
            chatInputSubmit.disabled = false;
            typing();
        }
    };
    chatInputContainer.appendChild(chatInputTextField);
    chatInputContainer.appendChild(chatInputSubmit);
    chatWindow.appendChild(chatInputContainer);
    chatWindow.appendChild(chatMessages);
    document.body.appendChild(chatWindow);
}

function typing() {
    if (typingIndicator) {
        return;
    }
    typingIndicator = document.createElement("div");
    typingIndicator.className = "messageBubble userMessage typing";
    chatMessages.appendChild(typingIndicator);
}

function notTyping() {
    typingIndicator && typingIndicator.remove()
    typingIndicator = null;
}


function submit() {
    let message = chatInputTextField.value
    postMessage(message, "user");
    respond(message);
}

function postMessage(message, sender) {
    let messageContainer = document.createElement("div");
    messageContainer.className = "message";
    let icon = document.createElement("div");
    icon.className = "icon " + sender + "Icon";
    let messageBubble = document.createElement("p");
    messageBubble.innerText = message;
    messageBubble.className = "messageBubble " + sender + "Message";
    messageContainer.appendChild(icon);
    messageContainer.appendChild(messageBubble);
    chatMessages.appendChild(messageContainer);
    chatMessages.scrollTop = chatMessages.scrollHeight;
    clearInput();

}

function clearInput() {
    chatInputTextField.value = '';
    chatInputSubmit.disabled = true;
    notTyping();
}

function respond(message) {
    let command = message.split(' ')[1];
    if (command === "start") {
        started = true;
        postMessage("Привет, меня зовут Чат-бот, а как зовут тебя?", "bot");
        return;
    }
    if (!started) {
        postMessage("Введите команду /start, для начала общения", "bot");
        return;
    }
    if (command === 'name') {
        let userName = message.split(' ')[2];
        postMessage(`Привет ${userName}, приятно познакомится. Я умею считать, введи числа которые надо посчитать`, "bot");
        return;
    }
    if (command === 'number') {
        let params = message.split(' ');
        [num1, num2] = [params[2], params[3]];
        postMessage("Пожалуйста выберите оперцию: -, +, * или /", "bot");
        return;
    }
    if (num1 && num2 && /[+\-*]/.test(message)) {
        postMessage(`The result is ${calculate[message](num1, num2)}`, "bot");
        [num1, num2] = [null, null];
        return;
    }
    if (command === "stop") {
        started = false;
        clearInput();
        postMessage("Всего доброго, если хочешь поговорить пиши /start", "bot");
        return;
    }
    postMessage("Я не понимаю, введите другую команду!", "bot");
}

